/*
 * Copyright (c) 2018-2020 Pavel Vasin
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

object Versions {
    const val kotlin = "1.3.72"
    const val coroutines = "1.3.8"
    const val serialization = "0.20.0"
    const val ktor = "1.3.2"
    const val weupnp = "0.1.4"
    const val bouncycastle = "1.66"
    const val leveldbjni = "1.18.3"
}
